%Recurrent Neural Network for Clone Detection
%Arseny Zorin, Vladimir Itsykson
%\today

# What is a clone

> Software clones are segments of code
> that are similar according to some 
> definition of similarity
  
> \- Ira Baxter, 2002

<!--
![StarWars](star_wars)
-->

# Causes of clones

* Copy \& paste
* Development of an existing implementation
* Multiple usage of a complex API
* Templates	usage

# Cons of clones

* Software maintenance
* Increasing the amount of a code
* Increasing resource requirements
* Increasing defect probability

# Types of clones

![Clones](clones)

 <!--
 **Type 1:** Identical fragments except comments and layout
 **Type 2:** Identical fragments except identifiers name and literal values
 **Type 3:** Syntactically similar fragments that differ at the statement level
 **Type 4:** Syntactically dissimilar fragments that implement the same functionality
-->


# Clone detection techniques

![Techniques](techniques)
<!--
* Classic methods:
	* Text-based
	* Token-based
	* Tree-based
	* Graph-based
	* Metric-based

* **AI-based**
-->

# Neural Networks usage

![Results](merge)

# What is RNN

Networks with loops in them, allowing information to presist.

![RNN](rnn)

# Why we use RNN

* Similarity tasks
* Processing tasks
* Recognition tasks
* etc.

# RNN Architecture

![Siam Arch](siam)

# Stages

![Stages](stages)

# Datasets generation

![Mutator](mutator)

# Bottlenecks

* Different size of input data 
* A lot of iterations on big projects

# Sequence-to-Sequence

![Sequence-to-Sequence](seq2seq)

# LSH

![LSH](lsh1)
![Hashing](lsh2)

# Current results

* AI-based method for clone detection was developed
* Tool for clone detection is in progress
* Clone detection with 80% accuracy
* Detection on small and medium sized projects
* Training lasts for 5 hours

# Future work

* Implement LSH for iteration reduce
* Improve the dataset
* Compare with existing utils
* Use BigCloneBench for testing
* Testing

# Contacts

Zorin Arseny

* ***E-mail:*** zorinarseny@yandex.ru
* ***Github:*** github.com/ArsenyZorin
* ***Gitlab:*** gitlab.com/ArsenyZorin
